/*
	We will create our own module which will have methods that will help us authorize our users to access or disallow access to certain parts/features in our app.	
*/

//imports
const jwt = require("jsonwebtoken");
const secret = "CourseBookingApi"

/*
	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access to certain parts of our app.

	JWT is like a gift wrapping service able to encode our user details which can only be unwrapped by jwt's own methods and if the secret provided is intact.

	If the JWT seemed tampered with, we will reject the user's attempt to access a feature in our app.
*/

module.exports.createAccessToken =(user) => {

	//check if we can receive the details of the foundUser in our login:
	//console.log(user)
	//data object is created to contain some details of our user
	const  data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//console.log(data)
	//create our jwt with data payload, our secret and the algorithm to create our JWT token

	return jwt.sign(data,secret,{})
}

/*
	Notes on JWT:

	1. You can only get a unique jwt with our secret if you log in to our app with the correct email and password.
	2. As a user, you can only get your own details from your own token from logging in.
	3. JWT is not meant to store sensitive data. For now, for ease of use and for our MVP, we add the email and isAdmin details of the logged in user, however, in the future, you can limit this to only the id and for every route and feature, you can simply lookup for the user in the database to get his details.
	4. JWT is like a more secure passport you use around the app to access certain features meant for your type of user.
	5. We will verify the legitimacy of a JWT every time a user access a restricted feature. Each JWT contains a secret only our server knows. If the jwt has been, in any way, changed, we will reject the user and his tampered token. If the jwt, does not contain a secret OR the secret is different, we will reject their access and token.
*/

module.exports.verify = (req,res,next) => {

	//middlewares which have access to req,res and next can also send responses to the client.

	//req.headers.authorization contains sensitive data and especially our token/jwt.
	//console.log(req.headers.authorization)
	let token = req.headers.authorization

	//If we are not passing a token in our request authorization or in our postman authorization, then here in our api, req.headers.authorization will be undefined.

	//This if statement will first check if token variable contains undefined or a proper jwt. If it is undefined, we will check token's data with typeof, then send a message to the client.
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."})
	} else {

		//We will check if the token is legit before proceeding to the next middleware/controller
		//console.log(token)//token before slice
		/*
			slice() is a method which can be used on strings and arrays.

			This will allow us to copy a part of the string.

			slice(<startingPosition>,<endPosition>)

			Starting position indicates the index number we will copy from.

			End position indicated the index number we will copy up to but not including the item in that position.

			Essentially, we extracted just the jwt token and re-assigned it to our token variable.
		*/
		token = token.slice(7,token.length)
		//console.log(token)

		//verify the legitimacy of token:
		jwt.verify(token,secret,function(err,decodedToken){
			//err will contain the error from decoding your token. This will contain the reason why we will reject the token.
			//If verification of the token is a success, then jwt.verify will return the decoded token.
			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				//console.log(decodedToken)//contains the data from our token
				//We will add a new property to the request object. In fact, anything that we add to the request object, we can still get from our next middleware or controller.
				req.user = decodedToken
				//user property will be added to request object and will contain our decodedToken. It can be accessed in the next middleware/controller.

				//next() will let us proceed to the next middleware OR controller.
				next()
			}
		})
	}
}

//verifyAdmin will also be used as a middleware
module.exports.verifyAdmin = (req,res,next) => {
	//verifyAdmin comes after the verify middleware
	//Do we have any access to our user's isAdmin detail?
	//We can get details from req.user because verifyAdmin comes after verify method.
	//Note: You can only have req.user for any middleware or controller that comes after verify.
	//console.log(req.user)

	if(req.user.isAdmin){
		//if the logged in user, based on his token is an admin, we will process to the next middleware/controller
		next()
	} else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}

