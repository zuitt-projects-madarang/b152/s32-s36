//import Course model
const Course = require('../models/Course')

module.exports.addCourseController = (req,res) => {
	//console.log(req.body)

	let newCourse = new Course ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newCourse.save()
	.then(course => res.send(course))
	.catch(error => res.send(error))
}

module.exports.getAllCoursesController = (req,res) => {
	Course.find({})
	.then(courses => res.send(courses))
	.catch(error => res.send(error))
}

module.exports.getSingleCourse = (req,res) => {
	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//update course: name, description and price
module.exports.updateCourse = (req,res) => {
	//console.log(req.params.id)//Where can we get our course's id?
	//console.log(req.body)//contains our new values.

	//updates object will contain the field/fields to update and its new value
	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	//use findByIdAndUpdate() to update our course
	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))
}

module.exports.archiveCourse = (req,res) => {
	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(archivedCourse => res.send(archivedCourse))
	.catch(err => res.send(err))
}

module.exports.activateCourse = (req,res) => {
	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(err => res.send(err))
}

module.exports.getActiveCourses = (req,res) => {
	Course.find({isActive: true})
	.then(actives => res.send(actives))
	.catch(err => res.send(err))
}

module.exports.getInactiveCourses = (req,res) => {
	Course.find({isActive: false})
	.then(inactives => res.send(inactives))
	.catch(err => res.send(err))
}

module.exports.checkCourseExists = (req,res) => {
	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {
		if(result.length === 0){
			return res.send("No Courses found.")
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err))
}

module.exports.checkSamePrice = (req,res) => {
	Course.find({price: req.body.price})
	.then(result => {
		if(result.length === 0){
			return res.send("No Courses found.")
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err))
}

module.exports.getEnrollees = (req,res) => {
	Course.findById(req.params.id)
	.then(result => res.send(result.enrollees))
	.catch(err => res.send(err))
}