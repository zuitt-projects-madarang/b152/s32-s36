//import User model
const User = require('../models/User')

//import Course model
const Course = require('../models/Course')

//import bcrypt
const bcrypt = require("bcrypt")
//bcrypt is a package which will help us add a layer of security for our user's passwords.

//auth module
const auth = require("../auth")
//console.log(auth)//this is our own auth js module

module.exports.registerUser = (req,res) => {

	//Does this controller need user input?
	//yes.
	//How/Where can we get our user's input?
	//req.body

	console.log(req.body)

	/*
		bcrypt adds a layer of security to our user's password.

		What bcrypt does is hash our password into a randomized character version of the original string.

		It is able to hide your password within that randomized string.
		
		syntax:
		bcrypt.hashSync(<stringToBeHashed><saltRounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized.
	*/

	//Create a variable to store the hashed/encrypted password. Then this new hashed password will be what we save in our database.
	const hashedPW = bcrypt.hashSync(req.body.password,10)
	//console.log(hashedPW);// If you want to check if the password was hashed.

	//Create a new user document out of our User mondel:
	let newUser = new User({

		//Check your model, check the fields you need in your new document.
		//Check your req.body if you have all the values needed for each field.
		//Add hashedPW to add the hashed password in the new document.

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo
	})

	//newUser is our new document created out of our User model.
	//new documents created out of the User model have access to some methods.
	//.save() method will allow us to save our new document into the collection that our models are connected to.
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))
}

module.exports.getAllUserController = (req,res) => {
	User.find({})
	.then(users => res.send(users))
	.catch(err => res.send(err))
}

module.exports.loginUser = (req,res) => {
	//Does this controller need user input?
	//yes
	//Where can we get this user input?
	//req.body
	//console.log(req.body)//contains user credentials

	/*
		1. Find the user by the email.
		2. If we found a user, we will check his password.
		3. If we don't find a user, we will send a message to the client.
		4. If upon checking the found user's password is the same as our input password, we will generate a "key" to access our app. If not, we will turn them away by sending a message in the client.
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {
		//console.log(foundUser)
		if(foundUser === null){
			return res.send("No User Found.")
		} else {
			//if we find a user, the foundUser parameter will contain the details of the document that matched our req.body.email
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password)
			console.log(isPasswordCorrect)
			/*
				syntax:
				bcrypt.compareSync(<string>,<hashedString>)

				If the string and the hashedString matches, compareSync method from bcrypt will return true, if not, false.
			*/
			if(isPasswordCorrect){
				/*
					auth.createAccessToken will receive our foundUser as an argument. Then, in the createAccessToken() will return a "token" or "key" which contains some of our user's details.

					This key is encoded and only our own methods will be able to decode it.

					auth.createAccessToken will return a JWT token out of our user details. Then, with res.send() we will be able to send the token to our client.

					This token or key will now let us allow or disallow users to access certain features of our app depending if they are admins or non-admins.
				*/
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("Incorrect Password.")
			}
		}
	})
	.catch(err => res.send(err))
}

//allow us to get the details of our logged in user
module.exports.getUserDetails = (req,res) => {

	/*
		req.user contains details from our decodedToken because the route for this controller uses verify as a middleware. Routes that do not use verify will not have access to req.user.
	*/

	console.log(req.user)

	//Find our logged in user's document from our db and send it to the client by its id.
	//Model.findById() will allow us to look for a document using an id
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
	//res.send("testing for verify")
}

module.exports.checkEmailExists = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send("Email is available.")
		} else {
			return res.send("Email is already registered!")
		}
	})
	.catch(error => res.send(error))
}

module.exports.updateUserDetails = (req,res) => {
	console.log(req.body)//input for new values for our user's details.
	console.log(req.user.id)//check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
}

module.exports.updateAdmin = (req,res) => {
	console.log(req.params.id)

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(adminUser => res.send(adminUser))
	.catch(err => res.send(err))
}

//enroll user

module.exports.enroll = async (req,res) => {
	//console.log("test enroll route")

	/*
		Enrollment Steps:

		1. Look for the user by their id.
			-push the details of the course we're trying to enroll in. We'll push to a new enrollment subdocument in our user.
		2. Look for the course by its id.
			-push the details of the enrollee/user who's trying to enroll. We'll push to a new enrollees subdocument in our course.
		3. When both saving of document are successful, we send a message to the client.
	*/

	//console.log(req.user.id)//the user's id from the decoded token after verify()
	//console.log(req.body.courseId)//the c ourse id from our request body

	//process stops here and sends response IF user is an admin.
	if(req.user.isAdmin){
		return res.send("Action Forbidden.")
	}

	//If not, we continue to our next steps:

	//Find the user:

	/*
		async - async keyword allows us to make our function asynchronous. Which means, that instead of JS regular behaviour of running each code line by line, it will allow us to wait for the result of functions.

		To wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish before proceeding.
	*/

	//return a boolean to our isUserUpdated variable as a result of our query and after saving our courseId into our user's enrollment subdocument array.
	let isUserUpdated = await User.findById(req.user.id).then(user => {

		//check if you found the user's documenet:
		//console.log(user)

		//Add the courseId in an object and push that object into the user's enrollments array:
		let newEnrollment = {
			courseId: req.body.courseId
		}

		//access the enrollment array from our user and push the new enrollment object into the enrollments array:
		//objects we push into subdocument array must also follow the scheme of the sudcoumment array in our User model.
		user.enrollments.push(newEnrollment)

		//save the changed made to our user document and return the value of saving our document.
		//If we properly saved our document, isUserUpdate will contain the boolean true.
		//If we catch an error, isUserUpdated will contain the error message.
		return user.save().then(user => true).catch(err => err.message)
	})

	//if isUserUpdated contains the boolean true, then, the saving of your user document is successful.
	//console.log(isUserUpdated)
	//if isUserUpdated does not contain the boolean true, we will stop our process and return a res.send() to our client with our message.

	if(isUserUpdated !== true) {
		return res.send({message: isUserUpdated})
	}

	//Find the course we will push our enrollee in then send return true as the value of isCourseUpdated. If isCourseUpdate is not equal to true, we will send the error message to our client. If it is, we will check both isUserUpdated and isCourseUpdated and return a message.

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		//console.log(course)
		//create an object which will contain details of our enrollees:
		let enrollee = {
			userId: req.user.id
		}
		//push the enrollee into the enrollees subdocument array of our course:
		course.enrollees.push(enrollee)

		//save the course document and our update
		//return true as value of isCourseUpdated if we save properly.
		//return the err.message if we catch an error.
		return course.save().then(course => true).catch(err => err.message)
	})
	//console.log(isCourseUpdated)

	//stop the process if there was an error saving our course document
	if(isCourseUpdated !== true) {
		return res.send({message: isCourseUpdated})
	}

	//send a message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true
	if(isUserUpdated && isCourseUpdated){
		return res.send({message:"Enrolled Successfully"})
	}
}

module.exports.getEnrollments = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err))
}