const express = require("express")
const mongoose = require("mongoose")
const app = express()
const port = 4000

//connect api to db:
mongoose.connect('mongodb+srv://aidz113:Hesoyam69420@cluster0.tuthz.mongodb.net/bookingAPI152?retryWrites=true&w=majority',
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//notifications for successful or failed mongodb connection:
let db = mongoose.connection
db.on("error",console.error.bind(console, "Connection Error"))
db.once("open",()=>console.log("Connected to MongoDB"))

app.use(express.json())

//import routes from userRoutes
const userRoutes = require("./routes/userRoutes")
//use our routes and group them together under '/users'
app.use('/users',userRoutes)

//import route from courseRoutes
const courseRoutes = require("./routes/courseRoutes")
//use our routes and group them together under '/courses'
app.use('/courses',courseRoutes)

app.listen(port,()=>console.log(`Server is running at port ${port}`));