const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true,"First name is required."]
	},
	lastName: {
		type: String,
		required: [true,"Last name is required."]
	},
	email: {
		type: String,
		required: [true,"Email is required."]
	},
	password: {
		type: String,
		required: [true,"Password is required."]
	},
	mobileNo: {
		type: String,
		required: [true,"Mobile number is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		
		/*
			In mongodb, we can have 2 way embedding for models with many to many relationship. To implement 2 way embedding, we add subdocument arrays for both models. Each subdocument array should also have a schema for its subdocuments which are the associative entities created.
		*/	
		{
			courseId: {
				type: String,
				required: [true,"Course ID is required."]
			},
			dateEnrolled: {
				type: Date, 
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
})

module.exports = mongoose.model("User",userSchema)