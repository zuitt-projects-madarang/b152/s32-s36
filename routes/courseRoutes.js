const express = require("express")
const router = express.Router()

//import course controller
const courseControllers = require('../controllers/courseControllers')
const auth = require("../auth")
const {verify,verifyAdmin} = auth

//routes

//Create/add course
//Since this route should not be accessed by all users, we have to authorize only some logged in users who have the proper authority.
//First, verify if the token the user is using is legit.
router.post('/',verify,verifyAdmin,courseControllers.addCourseController)

//get all courses
router.get('/',courseControllers.getAllCoursesController)

//get single course
router.get('/getSingleCourse/:id',courseControllers.getSingleCourse)

//update course
router.put('/:id',verify,verifyAdmin,courseControllers.updateCourse)

router.put('/archive/:id',verify,verifyAdmin,courseControllers.archiveCourse)

router.put('/activate/:id',verify,verifyAdmin,courseControllers.activateCourse)

router.get('/getActiveCourses',courseControllers.getActiveCourses)

router.get('/getInactiveCourses',verify,verifyAdmin,courseControllers.getInactiveCourses)

router.post('/checkCourseExists',courseControllers.checkCourseExists)

router.post('/checkSamePrice',courseControllers.checkSamePrice)

router.get('/getEnrollees/:id',verify,verifyAdmin,courseControllers.getEnrollees)

module.exports = router