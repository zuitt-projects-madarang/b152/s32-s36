const express = require("express")
const router = express.Router()

//import user controllers
const userControllers = require("../controllers/userControllers")

//We will import auth module so we can use our verify and verifyAdmin method as middleware for our routes.
const auth = require("../auth")
//destructure verify and verifyAdmin from auth. auth, is an imported module so therefore, it is an object in JS.
const {verify,verifyAdmin} = auth

//console.log(verify)

//User Registration:
router.post('/',userControllers.registerUser)

router.get('/',userControllers.getAllUserController)

//user login
router.post('/login',userControllers.loginUser)

//get logged in users details
//verify method from auth will be used as a middleware. Middleware are functions we can use around our app. In the context of expressjs route, middleware are functions we add in the route and can receive the request, response and next objects. We can have more than 1 middleware before the controller.
//router.get('<endpoint>,middleware,controller')
//When a route has a middleware and controller, we will run through the middleware first before we get to the controller.
//Note: Routes that have verify as a middleware would require us to pass a token from postman.
router.get('/getUserDetails',verify,userControllers.getUserDetails)

router.post('/checkEmailExists',userControllers.checkEmailExists)

router.put('/updateUserDetails',verify,userControllers.updateUserDetails)

router.put('/updateAdmin/:id',verify,verifyAdmin,userControllers.updateAdmin)

//enroll our registered users
router.post('/enroll',verify,userControllers.enroll)

router.get('/getEnrollments',verify,userControllers.getEnrollments)

module.exports = router